import React, { useReducer } from "react";
import { StyleSheet, View } from "react-native";

import ColorComponent from "../components/ColorComponent";
import { VerticalSpacer } from "../components/Spacers";
const COLOR_INCREMENT = 15;

const reducer = (state, action) => {
  switch (action.type) {
    case "change_red":
      return state.red + action.payload > 255 || state.red + action.payload < 0
        ? state
        : { ...state, red: state.red + action.payload };
    case "change_green":
      return state.green + action.payload > 255 ||
        state.green + action.payload < 0
        ? state
        : { ...state, green: state.green + action.payload };
    case "change_blue":
      return state.blue + action.payload > 255 ||
        state.blue + action.payload < 0
        ? state
        : { ...state, blue: state.blue + action.payload };
    default:
      return state;
  }
};
const SquareScreenUsingReducer = () => {
  const [state, dispatch] = useReducer(reducer, { red: 0, green: 0, blue: 0 });
  const { red, green, blue } = state;
  return (
    <View style={styles.mainContainer}>
      <VerticalSpacer space={80} />
      <ColorComponent
        color={"Red"}
        increment={() => {
          dispatch({ type: "change_red", payload: COLOR_INCREMENT });
        }}
        decrement={() => {
          dispatch({ type: "change_red", payload: -1 * COLOR_INCREMENT });
        }}
      />

      <ColorComponent
        color={"Green"}
        increment={() => {
          dispatch({ type: "change_green", payload: COLOR_INCREMENT });
        }}
        decrement={() => {
          dispatch({ type: "change_green", payload: -1 * COLOR_INCREMENT });
        }}
      />
      <ColorComponent
        color={"Blue"}
        increment={() => {
          dispatch({ type: "change_blue", payload: COLOR_INCREMENT });
        }}
        decrement={() => {
          dispatch({ type: "change_blue", payload: -1 * COLOR_INCREMENT });
        }}
      />

      <View
        style={{
          height: 100,
          width: 100,
          margin: 20,
          backgroundColor: `rgb(${red},${green},${blue})`,
        }}
      ></View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    alignSelf: "center",
    alignItems: "center",
  },
  inputTopContainer: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#DCE7FC",
    borderWidth: "1px solid black",
    height: 40,
    width: 200,
  },
  inputBottomContainer: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#DCE7FC",
    borderWidth: "1px solid black",
    borderTopColor: "white",
    height: 40,
    width: 200,
  },
  counterTextStyle: {
    fontSize: 40,
  },
});

export default SquareScreenUsingReducer;
