import React, { useState } from "react";
import { TextInput, View, Text, StyleSheet } from "react-native";

const BoxScreen = () => {
  return (
    <View style={styles.mainView}>
      <Text style={styles.textStyle}>Box Screen </Text>
    </View>
  );
};
const styles = StyleSheet.create({
  mainView: {
    margin: 15,
    borderWidth: 3,
    borderColor: "black",
  },
  textStyle: {
    margin: 10,
    padding: 10,
    borderWidth: 1,
    borderColor: "red",
  },
});
export default BoxScreen;
