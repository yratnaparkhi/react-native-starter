import React, { useReducer } from "react";
import { Text, StyleSheet, View, TouchableOpacity } from "react-native";
import { VerticalSpacer } from "../components/Spacers";

const reducer = (state, action) => {
  switch (action.type) {
    case "increase":
      return { ...state, counter: state.counter + action.payload };
    case "decrease":
      return state.counter == 0
        ? state
        : { ...state, counter: state.counter - action.payload };
    default:
      return state;
  }
};

const CounterScreenUsingReducer = () => {
  const [state, dispatch] = useReducer(reducer, { counter: 0 });
  const { counter } = state;
  return (
    <View style={styles.mainContainer}>
      <VerticalSpacer space={80} />
      <TouchableOpacity
        onPress={() => {
          dispatch({ type: "increase", payload: 1 });
        }}
      >
        <LabelComponent title={"Increase"} />
      </TouchableOpacity>
      <VerticalSpacer space={20} />
      <TouchableOpacity
        onPress={() => {
          dispatch({ type: "decrease", payload: 1 });
        }}
      >
        <LabelComponent title={"Decrease"} />
      </TouchableOpacity>

      <VerticalSpacer space={40} />
      <Text>Current Count: </Text>

      <VerticalSpacer space={40} />
      <Text style={styles.counterTextStyle}>{counter} </Text>
      <VerticalSpacer space={80} />
    </View>
  );
};

const LabelComponent = ({ title }) => {
  return (
    <View style={styles.inputContainer}>
      <Text>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    alignSelf: "center",
    alignItems: "center",
  },
  inputContainer: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#DCE7FC",
    height: 40,
    width: 200,
  },
  counterTextStyle: {
    fontSize: 40,
  },
});

export default CounterScreenUsingReducer;
