import React from "react";
import { Text, View, StyleSheet, FlatList } from "react-native";

const ListScreen = () => {
  const friends = [
    { name: "friend 1", age: "10" },
    { name: "friend 2", age: "11" },
    { name: "friend 3", age: "12" },
    { name: "friend 4", age: "13" },
    { name: "friend 5", age: "14" },
    { name: "friend 6", age: "15" },
    { name: "friend 7", age: "16" },
    { name: "friend 11", age: "10" },
    { name: "friend 21", age: "11" },
    { name: "friend 31", age: "12" },
    { name: "friend 41", age: "13" },
    { name: "friend 51", age: "14" },
    { name: "friend 61", age: "15" },
    { name: "friend 71", age: "16" },
    { name: "friend 12", age: "10" },
    { name: "friend 22", age: "11" },
    { name: "friend 32", age: "12" },
    { name: "friend 42", age: "13" },
    { name: "friend 52", age: "14" },
    { name: "friend 62", age: "15" },
    { name: "friend 72", age: "16" },
    { name: "friend 13", age: "10" },
    { name: "friend 23", age: "11" },
    { name: "friend 33", age: "12" },
    { name: "friend 43", age: "13" },
    { name: "friend 53", age: "14" },
    { name: "friend 63", age: "15" },
    { name: "friend 73", age: "16" },
  ];
  console.log("hello");
  return (
    <FlatList
      //   horizontal={true}
      //   showsHorizontalScrollIndicator={true}
      data={friends}
      keyExtractor={(friend) => friend.name}
      renderItem={({ item }) => {
        return (
          <Text style={style.textStyle}>
            {item.name} - Age {item.age}
          </Text>
        );
      }}
    />
  );
};
const style = StyleSheet.create({
  textStyle: {
    fontSize: 20,

    margin: 20,
  },
});

export default ListScreen;
