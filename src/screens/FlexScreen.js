import React, { useState } from "react";
import { TextInput, View, Text, StyleSheet } from "react-native";

const FlexScreen = () => {
  return (
    <View style={styles.mainView}>
      <Text style={styles.textStyle1}>Flex child #1 </Text>
      <Text style={styles.textStyle2}>Flex child #2 </Text>
      <Text style={styles.textStyle3}>Flex child #3 </Text>
    </View>
  );
};
const styles = StyleSheet.create({
  mainView: {
    // alignItems: "flex-end",
    justifyContent: "center",
    height: 200,
    flexDirection: "row",
    margin: 15,
    borderWidth: 3,
    borderColor: "black",
  },
  textStyle1: {
    alignSelf: "flex-start",
    flex: 1,
    margin: 10,
    padding: 10,
    borderWidth: 1,
    borderColor: "red",
  },
  textStyle2: {
    flex: 2,
    margin: 10,
    padding: 10,
    borderWidth: 1,
    borderColor: "red",
  },
  textStyle3: {
    flex: 1,
    margin: 10,
    padding: 10,
    borderWidth: 1,
    borderColor: "red",
  },
});
export default FlexScreen;
