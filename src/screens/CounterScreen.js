import React, { useState } from "react";
import { Text, StyleSheet, View, TouchableOpacity } from "react-native";
import { VerticalSpacer } from "../components/Spacers";

const CounterScreen = () => {
  const [counter, setCounter] = useState(0);
  return (
    <View style={styles.mainContainer}>
      <VerticalSpacer space={80} />
      <TouchableOpacity
        onPress={() => {
          setCounter(counter + 1);
        }}
      >
        <LabelComponent title={"Increase"} />
      </TouchableOpacity>
      <VerticalSpacer space={20} />
      <TouchableOpacity
        onPress={() => {
          if (counter != 0) setCounter(counter - 1);
        }}
      >
        <LabelComponent title={"Decrease"} />
      </TouchableOpacity>

      <VerticalSpacer space={40} />
      <Text>Current Count: </Text>

      <VerticalSpacer space={40} />
      <Text style={styles.counterTextStyle}>{counter} </Text>
      <VerticalSpacer space={80} />
    </View>
  );
};

const LabelComponent = ({ title }) => {
  return (
    <View style={styles.inputContainer}>
      <Text>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    alignSelf: "center",
    alignItems: "center",
  },
  inputContainer: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#DCE7FC",
    height: 40,
    width: 200,
  },
  counterTextStyle: {
    fontSize: 40,
  },
});

export default CounterScreen;
