import React from "react";
import { Text, StyleSheet, Button, View, TouchableOpacity } from "react-native";
import { FlatList } from "react-native-gesture-handler";

const HomeScreen = ({ navigation }) => {
  const navigationList = [
    {
      title: "Go to component demo page",
      goTo: "Components",
    },
    {
      title: "Go to list page",
      goTo: "List",
    },
    {
      title: "Go to image page",
      goTo: "Image",
    },

    {
      title: "Go to counter page",
      goTo: "Counter",
    },
    {
      title: "Counter page using reducer",
      goTo: "CounterReducer",
    },
    {
      title: "Go to color page",
      goTo: "Color",
    },
    {
      title: "Go to square page",
      goTo: "Square",
    },
    {
      title: "Square page using reducer",
      goTo: "SquareReducer",
    },
    {
      title: "Go to Text Screen",
      goTo: "TextScreen",
    },
    {
      title: "Go to Box Screen",
      goTo: "Box",
    },
    {
      title: "Go to Flex Screen",
      goTo: "Flex",
    },
  ];

  return (
    <View>
      <Text style={styles.text}>This is Home Screen</Text>
      <FlatList
        data={navigationList}
        renderItem={({ item }) => {
          return (
            <Button
              title={item.title}
              onPress={() => navigation.navigate(item.goTo)}
              style={styles.buttonStyle}
            />
          );
        }}
        inverted
      />
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    alignSelf: "center",
    marginBottom: 20,
    marginTop: 20,
  },
  buttonStyle: {
    paddingVertical: 60,
  },
});

export default HomeScreen;
