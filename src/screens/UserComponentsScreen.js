import React from "react";
import { Text, StyleSheet, View } from "react-native";

const UserComponentsScreen = () => {
  const username = "Yash";
  return (
    <View>
      <Text style={styles.headerStyle}>Getting started with react native</Text>
      <Text style={styles.contentStyle}>My name is {username}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  headerStyle: {
    fontSize: 45,
  },
  contentStyle: {
    fontSize: 20,
  },
});

export default UserComponentsScreen;
