import React, { useState } from "react";
import { StyleSheet, View, Text } from "react-native";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import { VerticalSpacer } from "../components/Spacers";

const ColorScreen = () => {
  const [colors, setColors] = useState([]);
  return (
    <View>
      <VerticalSpacer space={80} />
      <TouchableOpacity
        onPress={() => {
          setColors([...colors, randomRgb]);
        }}
      >
        <View style={styles.buttonStyle}>
          <Text>Add Color</Text>
        </View>
      </TouchableOpacity>
      <VerticalSpacer space={40} />

      <FlatList
        data={colors}
        keyExtractor={(item) => "item"}
        renderItem={() => {
          return (
            <View
              style={{
                height: 100,
                width: 100,
                margin: 20,
                backgroundColor: randomRgb(),
              }}
            ></View>
          );
        }}
      />
    </View>
  );
};

const randomRgb = () => {
  const red = Math.floor(Math.random() * 256);
  const blue = Math.floor(Math.random() * 256);
  const green = Math.floor(Math.random() * 256);
  return `rgb(${red}, ${green}, ${blue})`;
};
const styles = StyleSheet.create({
  buttonStyle: {
    height: 40,
    width: 200,
    backgroundColor: "#DCE7FC",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
  },
  blockDisplay: {
    backgroundColor: "red",
    alignSelf: "center",
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    flex: 1,
  },
});

export default ColorScreen;
