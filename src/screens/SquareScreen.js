import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { VerticalSpacer } from "../components/Spacers";
import ColorComponent from "../components/ColorComponent";
const COLOR_INCREMENT = 15;
const SquareScreen = () => {
  const [redColor, setRedColor] = useState(0);
  const [blueColor, setBlueColor] = useState(0);
  const [greenColor, setGreenColor] = useState(0);

  const setColor = (color, changeBy) => {
    switch (color) {
      case "red":
        redColor + changeBy > 255 || redColor + changeBy < 0
          ? null
          : setRedColor(redColor + changeBy);
        return;
      case "green":
        greenColor + changeBy > 255 || greenColor + changeBy < 0
          ? null
          : setGreenColor(greenColor + changeBy);
        return;
      case "blue":
        blueColor + changeBy > 255 || blueColor + changeBy < 0
          ? null
          : setBlueColor(blueColor + changeBy);
        return;
      default:
        return;
    }
  };
  return (
    <View style={styles.mainContainer}>
      <VerticalSpacer space={80} />
      <ColorComponent
        color={"Red"}
        increment={() => setColor("red", COLOR_INCREMENT)}
        decrement={() => setColor("red", -1 * COLOR_INCREMENT)}
      />
      {console.log(redColor)}
      <ColorComponent
        color={"Green"}
        increment={() => setColor("green", COLOR_INCREMENT)}
        decrement={() => setColor("green", -1 * COLOR_INCREMENT)}
      />
      <ColorComponent
        color={"Blue"}
        increment={() => setColor("blue", COLOR_INCREMENT)}
        decrement={() => setColor("blue", -1 * COLOR_INCREMENT)}
      />

      <View
        style={{
          height: 100,
          width: 100,
          margin: 20,
          backgroundColor: `rgb(${redColor},${greenColor},${blueColor})`,
        }}
      ></View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    alignSelf: "center",
    alignItems: "center",
  },
  inputTopContainer: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#DCE7FC",
    borderWidth: "1px solid black",
    height: 40,
    width: 200,
  },
  inputBottomContainer: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#DCE7FC",
    borderWidth: "1px solid black",
    borderTopColor: "white",
    height: 40,
    width: 200,
  },
  counterTextStyle: {
    fontSize: 40,
  },
});

export default SquareScreen;
