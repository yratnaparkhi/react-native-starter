import React from "react";
import { Text, StyleSheet } from "react-native";

const ComponentsScreen = () => {
  return <Text style={styles.textStyle}> Hi this is the components page</Text>;
};

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 30,
  },
  horizontalSpacer40: {
    height: 40,
  },
});

export default ComponentsScreen;
