import React, { useState } from "react";
import { TextInput, View, Text, StyleSheet } from "react-native";

const TextScreen = () => {
  const [name, setName] = useState("");
  return (
    <View style={styles.mainView}>
      <TextInput
        style={styles.textInput}
        autoCorrect={false}
        autoCapitalize="none"
        value={name}
        onChangeText={(newName) => setName(newName)}
      />
      {name.length < 5 ? (
        <Text>{"Name must be of al least 5 characters"}</Text>
      ) : null}
    </View>
  );
};
const styles = StyleSheet.create({
  textInput: {
    marginVertical: 15,
    height: 50,
    color: "black",
    borderWidth: 1,
    padding: 10,
  },
  mainView: {
    margin: 15,
  },
});
export default TextScreen;
