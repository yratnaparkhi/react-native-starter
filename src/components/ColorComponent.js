import React from "react";
import { Text, View, StyleSheet } from "react-native";
import { VerticalSpacer } from "../components/Spacers";
import LabelComponent from "./LabelComponent";
const ColorComponent = ({ color, increment, decrement }) => {
  return (
    <View>
      <Text>{color}</Text>
      <LabelComponent title={`More ${color}`} onPress={() => increment()} style={styles.inputTopContainer} />
      <LabelComponent title={`Less ${color}`} onPress={() => decrement()} style={styles.inputBottomContainer} />
      <VerticalSpacer space={40} />
    </View>
  );
};
const styles = StyleSheet.create({
  inputTopContainer: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#DCE7FC",
    borderWidth: "1px solid black",
    height: 40,
    width: 200,
  },
  inputBottomContainer: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#DCE7FC",
    borderWidth: "1px solid black",
    borderTopColor: "white",
    height: 40,
    width: 200,
  },
});
export default ColorComponent;
