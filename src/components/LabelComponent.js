import React from "react";
import { Text, View, TouchableOpacity } from "react-native";

const LabelComponent = ({ title, onPress, style }) => {
  return (
    <TouchableOpacity onPress={() => onPress()}>
      <View style={style}>
        <Text>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default LabelComponent;
