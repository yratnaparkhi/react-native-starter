import { View } from "react-native";

// export const HorizontalSpacer = ({ space }) => {
//   return <View style={{ width: space }}></View>;
// };
export const VerticalSpacer = ({ space }) => {
  return <View style={{ height: space }}></View>;
};
