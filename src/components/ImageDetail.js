import React from "react";
import { View, Text, StyleSheet, Image } from "react-native";

const ImageDetail = (props) => {
  console.log(props);
  return (
    <View style={styles.containerStyle}>
      <Image source={props.imageSource} />
      <Text style={styles.textStyle}>{props.title}</Text>
      <Text style={styles.textStyle}>Image Score: {props.imageScore}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 20,
  },
  containerStyle: {
    margin: 20,
  },
});

export default ImageDetail;
